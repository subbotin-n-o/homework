package employee;

import animals.*;
import food.Food;
import food.WrongFoodException;

public class Worker{
    /*
    * С помощью конструкции try-catch обработать все вызовы метода eat.
    * Результатом обработки исключения должен быть вывод stack trace.
    */

    public void feed(Animal animal, Food food) {    //покормить любое животное любой едой
        try {
            animal.eat(food);
        }
        catch (WrongFoodException e) {
            e.printStackTrace();
        }
    }

    public void getVoice(Voice voice) {     //заставить животное подать голос
        voice.getVoice();
    }
}