import animals.*;
import employee.Worker;
import food.Grass;
import food.Meat;
import model.Aviary;
import model.Size;

public class Zoo {

    private static Aviary<Carnivorous> carnivorousAviary = new Aviary<>(Size.MEDIUM);
    private static Aviary<Herbivore> herbivoreAviary = new Aviary<>(Size.LARGE);

    public static void main(String[] args) {
        Herbivore duck = new Duck("Утка");                       //Объект класса Duck
        Herbivore hippopotamus = new Hippopotamus("Бегемот");    //Объект класса Hippopotamus
        Herbivore giraffe = new Giraffe("Жираф");                //Объект класса Giraffe
        Carnivorous fish = new Fish("Рыба");                     //Объект класса Fish
        Carnivorous kotik = new Kotik("Кот");                    //Объект класса Kotik
        Carnivorous dog = new Dog("Собака");                     //Объект класса Dog
        Grass grass = new Grass();                      //Объект класса Grass
        Meat meat = new Meat();                         //Объект класса Meat
        Worker worker = new Worker();                   //Объект класса Worker

        worker.feed(duck, grass);                       //Покормить утку травой
        worker.feed(hippopotamus, meat);                //Покормить cлона мясом
        worker.feed(giraffe, grass);
        worker.feed(fish, grass);
        worker.feed(kotik, meat);
        worker.feed(dog, grass);
        worker.getVoice((Voice) duck);              //заставить утку подать голос
        worker.getVoice((Voice) hippopotamus);      //заставить бегемота подать голос
        worker.getVoice((Voice) kotik);             //заставить котика подать голос
        worker.getVoice((Voice) dog);              //заставить собаку подать голос

        for (int i = 0; i < createPond().length; i++);
        createPond()[0].swim();
        createPond()[1].swim();
    }

    public static Swim[] createPond() {
        Swim[] array = new Swim[2];
        array[0] = new Duck("Рыба");
        array[1] = new Fish("Рыба");
        return array;
    }

    public static void fillCarnivorousAviary() {
        Carnivorous dog1 = new Dog("Псина");
        Carnivorous kotik1 = new Kotik("Котяра");
        carnivorousAviary.addAnimal(dog1);
        carnivorousAviary.addAnimal(kotik1);
    }

    public static void fillHerbivoreAviary() {
        Herbivore duck1 = new Duck("Утка");
        Herbivore giraffe1 = new Duck("Жираф");
        herbivoreAviary.addAnimal(duck1);
        herbivoreAviary.addAnimal(giraffe1);
    }

    public static Carnivorous getCarnivorous(String name) {
        return (Carnivorous) carnivorousAviary.getAnimal(name);
    }

    public static Herbivore getHerbivore(String name) {
        return (Herbivore) herbivoreAviary.getAnimal(name);
    }

}