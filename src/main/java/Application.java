import animals.Kotik;

public class Application {

    public static void main(String[] args) {

        Kotik kotik1 = new Kotik("Степан", "Мяу-мяу", 5, 5000);

        Kotik kotik2 = new Kotik("Игорь");

        kotik2.setName("Рыжий");
        kotik2.setVoice("Мяу-мяу");
        kotik2.setSatiety(5);
        kotik2.setWeight(5000);

//        String StepanName = kotik2.getName();
//        String StepanVoice = kotik2.getVoice();
//        int StepanSatiety = kotik2.getSatiety();
//        int StepanWeight = kotik2.getWeight();

        for (String str : kotik2.liveAnotherDay()
        ) {
            System.out.println(str);
        }

//        System.out.println("Имя котика " + StepanName);
//        System.out.println("Вес котика " + StepanWeight);

        compareVoice(kotik1, kotik2);

        int count = Kotik.getCount();
        System.out.println(count);

    }

    static boolean compareVoice(Kotik kotik1, Kotik kotik2){
        String myau1 = kotik1.getVoice();
        String myau2 = kotik2.getVoice();
        if (myau1.equalsIgnoreCase(myau2)){
            System.out.println("Котики говорят Мяу-мяу");
            return true;
        } else {
            System.out.println("Котики говорят по разному");
            return false;
        }
    }
}
