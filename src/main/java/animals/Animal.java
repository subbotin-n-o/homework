package animals;

import food.Food;
import food.WrongFoodException;
import model.Size;

import java.util.UUID;

public abstract class Animal {

    private int satiety = 0;    //шкала сытости

    private String name;    //уникальный идентификатор - поле String name

    public Animal(String name) {
        this.name = name;
    }

    public abstract Size getSize();

    public String getName() {
        return name;
    }

    public abstract void eat(Food food) throws WrongFoodException; //Абстр. метод eat(Food food), кот. принимает в кач-ве арг-та объект еды

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getSatiety() {
        return satiety;
    }
}