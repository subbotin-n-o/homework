package animals;

import model.Size;

public class Kotik extends Carnivorous implements Voice, Run {
    private String name;        //имя
    private String voice;       //голос, как мяукает
    private int satiety;        //сытость
    private int weight;         //вес
    private String food;        //название еды

    private static int count;    //переменная, отв-я за кол-во созданных экземпляров класса.

    private static final int METHODS = 5;    //константа, соотв-я кол-ву методов поведения животного

    public Kotik (String name, String voice, int satiety, int weight) {
        super(name);

        this.name = name;
        this.voice = voice;
        this.satiety = satiety;
        this.weight = weight;
        ++count;
    }

    public Kotik(String name) {
        super(name);
        ++count;
    }

/*
    public Kotik () {
        ++count;
        this.name = "Степан";
        this.voice = "Мяу-мяу";
        this.satiety = 5;
        this.weight = 5000;
    }
*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVoice() {
        System.out.println("Котик говорит " + "Мяу-Мяу");
        return "Мяу-Мяу";
    }

    public void setVoice(String voice) {
        this.voice = voice;
    }

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public static int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getFood() {
        return food;
    }

    public void setFood(int count) {
        this.food = food;
    }

    //метод играть
    public boolean play () {
        if (this.satiety > 0){
            //System.out.println(this.name + " is playing");
            this.satiety--;
            return true;
        } else {
            //System.out.println(this.name + " просит кушать");
            return false;
        }
    }

    //метод спать
    public boolean sleep () {
        if (this.satiety > 0){
            //System.out.println(this.name + " is sleeping");
            this.satiety--;
            return true;
        } else {
            //System.out.println(this.name + " просит кушать");
            return false;
        }
    }

    //метод умываться
    public boolean wash () {
        if (this.satiety > 0){
            //System.out.println(this.name + " washes");
            this.satiety--;
            return true;
        } else {
            //System.out.println(this.name + " просит кушать");
            return false;
        }
    }

    //метод гулять
    public boolean walk () {
        if (this.satiety > 0){
            //System.out.println(this.name + " is walking");
            this.satiety--;
            return true;
        } else {
            //System.out.println(this.name + " просит кушать");
            return false;
        }
    }

    //метод охотиться
    public boolean hunt () {
        if (this.satiety > 0){
            //System.out.println(this.name + " to hunt");
            this.satiety--;
            return true;
        } else {
            //System.out.println(this.name + " просит кушать");
            return false;
        }
    }

    //три перегрузки метода eat()
    public void eat(int satiety){   //принимает кол-во усл. ед. сытости и ув-ет на них соотв-ую пер-ую экз.
        this.satiety += satiety;
        //System.out.println(this.satiety);
    }

    public void eat(int satiety, String food){  //принимает единицы сытости и название еды
        this.satiety += satiety;
        this.food = food;
        //System.out.println(this.satiety);
    }

    public void eat(){      //содержит внутри себя вызов перегрузки, при-ей ед. сытости и наз. еды.
        String food = this.food;
        int satiety = this.satiety;
        eat(satiety, food);
    }

    public void feedKotik(){
        //System.out.println("Кормим котика");
        this.satiety++;
    }

    public String[] liveAnotherDay(){
        String[] array = new String[24];
        for (int i = 0; i < array.length; i++){
            switch ((int) (Math.random() * METHODS) + 1){
                case 1:
                    if (play()){
                        array[i] = i + " - играл";
                    } else {
                        feedKotik();
                        //eat(2);
                        array[i] = i + " - покушал";
                    }
                    break;
                case 2:
                    if (sleep()){
                        array[i] = i + " - спал";
                    } else {
                        feedKotik();
                        //eat(2);
                        array[i] = i + " - покушал";
                    }
                    break;
                case 3:
                    if (wash()){
                        array[i] = i + " - умывался";
                    } else {
                        feedKotik();
                        //eat(2);
                        array[i] = i + " - покушал";
                    }
                    break;
                case 4:
                    if (walk()){
                        array[i] = i + " - гулял";
                    } else {
                        feedKotik();
                        //eat(2);
                        array[i] = i + " - покушал";
                    }
                    break;
                case 5:
                    if (hunt()){
                        array[i] = i + " - охотился";
                    } else {
                        feedKotik();
                        //eat(2);
                        array[i] = i + " - покушал";
                    }
                    break;
            }
        }
        return array;
    }

    @Override
    public void run() {
        System.out.println("Котик " + getName() + " умеет бегать");
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }
}