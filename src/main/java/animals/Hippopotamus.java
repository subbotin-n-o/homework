package animals;

import model.Size;
public class Hippopotamus extends Herbivore implements Run, Voice{

    public Hippopotamus(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Бегемот умеет бегать");
    }

    @Override
    public String getVoice() {
        System.out.println("Бегемот говорит " + "Эээээ");
        return "Эээээ";
    }

    @Override
    public Size getSize() {
        return Size.LARGE;
    }
}