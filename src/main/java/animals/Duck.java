package animals;

import model.Size;

public class Duck extends Herbivore implements Fly, Swim, Voice, Run{

    public Duck(String name) {
        super(name);
    }

    @Override
    public void fly() {
        System.out.println("Утка умеет летать");
    }

    @Override
    public boolean swim() {
        System.out.println("Утка умеет плавать");
        return false;
    }

    @Override
    public String getVoice() {
        System.out.println("Утка говорит " + "Кря-Кря");
        return "Кря-Кря";
    }

    @Override
    public void run() {
        System.out.println("Утка умеет бегать");
    }

    @Override
    public Size getSize() {
        return Size.SMALL;
    }
}
