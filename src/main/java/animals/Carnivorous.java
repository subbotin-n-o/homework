package animals;

import food.Food;
import food.Grass;
import food.WrongFoodException;

public abstract class Carnivorous extends Animal {
    public Carnivorous(String name) {
        super(name);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {
        if (food instanceof Grass) {
            System.out.println("Это животное такое не ест");
            throw new WrongFoodException();
        } else {
            setSatiety(food.getEnergy());
            System.out.println("Животное поело");
        }
    }
}