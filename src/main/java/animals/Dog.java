package animals;

import model.Size;

public class Dog extends Carnivorous implements Voice, Run, Swim{
    public Dog(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println("Собака умеет бегать");
    }

    @Override
    public boolean swim() {
        System.out.println("Собака умеет плавать");
        return false;
    }

    @Override
    public String getVoice() {
        System.out.println("Собака говорит " + "Гав-гав");
        return "Гав-гав";
    }

    @Override
    public Size getSize() {
        return Size.MEDIUM;
    }
}