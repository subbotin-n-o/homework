package model;

import animals.Animal;
import java.util.HashMap;

public class Aviary <Av extends Animal> {
    private Size size;
    private HashMap<String, Animal> aviaryMap = new HashMap<>();

    public <Av> Aviary(Size size) {
        this.size = size;
    }

    //addAnimal - добавить животное в вольер
    public <Av> void addAnimal(Animal animal) {
        if (animal.getSize() == size) {
            aviaryMap.put(animal.getName(), animal);
            System.out.println("Животное " + animal.getName() + " добавлено.");
        } else {
            throw new WrongSizeException();
        }
    }

    //getAnimal - получить ссылку на животное в вольере по name
    public Animal getAnimal(String name) {
        if (!aviaryMap.isEmpty()) {
            System.out.println(aviaryMap.containsKey(name));
        } else {
            System.out.println("Вольер пустой");
        }
        return null;
    }

    //removeAnimal - удалить животное из вольера по name
    public boolean removeAnimal(String name) {
        if (aviaryMap.containsKey(name)) {
            System.out.println(aviaryMap.remove(name) + " Животное" + " " + name + " удалено.");
            return true;
        }
        else System.out.println("Животного нет в вольере");
        return false;
    }
}